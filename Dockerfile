FROM alpine:3.12
ENV PYTHONUNBUFFERED=1
RUN echo "**** Installing Python ****" && \
    apk add --no-cache python3=3.8.5-r0 && \
    if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
    echo "**** Installing Pip ****" && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    echo "**** Installing uWSGI ****" && \
    echo "---- Installing dependecies ----" && \
    apk add --no-cache --update gcc libc-dev python3-dev musl-dev linux-headers make g++ && \
    echo "---- Installing pcre ----" && \
    wget https://ftp.pcre.org/pub/pcre/pcre-8.00.tar.gz && \
    tar -xzf pcre-8.00.tar.gz && rm pcre-8.00.tar.gz && \
    cd pcre-8.00 && ./configure && make && make install && \
    cd - && rm -rf pcre-8.00 && \
    echo "---- Installing uWSGI itself ----" && \
    wget https://github.com/unbit/uwsgi/archive/2.0.19.1.tar.gz && \
    tar -xzf 2.0.19.1.tar.gz && rm 2.0.19.1.tar.gz && \
    cd uwsgi-2.0.19.1 && \ 
    python3 uwsgiconfig.py --plugin plugins/python && \ 
    python3 uwsgiconfig.py --build && \
    cp ./python_plugin.so /usr/lib/python_plugin.so && \
    cp ./uwsgi /usr/bin/uwsgi && \
    cd - && rm -rf uwsgi-2.0.19.1 && \
    echo "---- Creating uwsgi user and log dir ----" && \
    adduser -H -D uwsgi && \
    mkdir -p /var/log/uwsgi && \
    echo "---- Installing uwsgitop ----" && \
    pip install --no-cache --upgrade uwsgitop && \
    if [ ! -e /usr/bin/uwsgitop ]; then ln -s uwsgitop /usr/bin/uwsgitop ; fi && \
    echo "---- Deleting requirements ----" && \
    apk del gcc libc-dev python3-dev musl-dev linux-headers make g++
