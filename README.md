## Docker image with python, uwsgi based on alpine

### Content
* Alpine 3.12.
* uWSGI 2.0.19.1 built with pcre support, python plugin. With uwsgi user created and /var/uwsgi/log directory.
* Python 3.8.5-r0 with pip 20.2.4, setuptools, wheel installed.
* uwsgitop of unspecified version to monitor health of the uwsgi.

### Availability 
Full size of the latest version (gtorianik/python-uwsgi:0.0.2) is 62.9MB. Compressed size is 19.37MB.

### Usage
```bash
$ docker pull gtorianik/python-uwsgi:0.0.2
```

*Inspired by https://github.com/Docker-Hub-frolvlad/docker-alpine-python3*
